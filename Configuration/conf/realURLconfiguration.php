<?php

$TYPO3_CONF_VARS['SC_OPTIONS'] ['t3lib/class.t3lib_tstemplate.php'] ['linkData-PostProc'] [] = 'EXT:realurl/class.tx_realurl.php:&tx_realurl->encodeSpURL';
$TYPO3_CONF_VARS['SC_OPTIONS'] ['tslib/class.tslib_fe.php'] ['checkAlternativeIdMethods-PostProc'] [] = 'EXT:realurl/class.tx_realurl.php:&tx_realurl->decodeSpURL';
$TYPO3_CONF_VARS['SC_OPTIONS'] ['t3lib/class.t3lib_tcemain.php'] ['clearAllCache_additionalTables'] ['tx_realurl_urldecodecache'] = 'tx_realurl_urldecodecache';
$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearAllCache_additionalTables']['tx_realurl_pathcache'] = 'tx_realurl_pathcache';

$TYPO3_CONF_VARS['FE'] ['addRootLineFields'] .= ',tx_realurl_pathsegment,alias,nav_title,title';

$TYPO3_CONF_VARS['EXTCONF'] ['realurl'] ['_DEFAULT'] = array(
    'init' => array(
        'enableCHashCache' => 1,
        'enableUrlDecodeCache' => 1,
        'enableUrlEncodeHash' => 1,
        'useCHashCache' => true,
        'appendMissingSlash' => 'ifNotFile',
        'adminJumpToBackend' => true,
        'enableUrlEncodeCache' => true,
        'emptyUrlReturnValue' => '/',
        'enableAllUnicodeLetters' => true,
        'postVarSet_failureMode' => 'ignore',
        'respectSimulateStaticURLs' => true ,
    ),

    'rewrite' => array(
    ),

    'pagePath' => array(
        'type' => 'user',
        'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'expireDays' => 3,
        'rootpage_id' => '1'
    ),

    'fileName' => array (
        'defaultToHTMLsuffixOnPrev' => 1,
        'index' => array(
            'page.html' => array(
                'keyValues' => array (
                    'type' => 1,
                ),
            ),
            'robots.txt' => array(
                'keyValues' => array(
                    'type' => 201,
                ),
            ),
        ),
    ),

	'fixedPostVars' => array(
		'mm_forum_pi3' => array(
			array(
				'GETvar'            => 'tx_mmforum_pi3[folder]',
				'valueMap'          => array(
					'posteingang'		=> 'inbox',
					'postausgang'		=> 'send',
					'archiv'			=> 'archiv'
				)
			),
			array(
				'GETvar'            => 'tx_mmforum_pi3[action]',
				'valueMap'          => array(
					'lesen'				=> 'message_read',
					'schreiben'			=> 'message_write',
					'loeschen'			=> 'message_del'
				)
			),
			array(
				'GETvar'            => 'tx_mmforum_pi3[messid]',
				'lookUpTable'       => array(
					'table'                 => 'tx_mmforum_pminbox' ,
					'id_field'              => 'uid',
					'alias_field'           => 'subject',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			),
			array(
				'GETvar'            => 'tx_mmforum_pi3[userid]',
				'lookUpTable'       => array(
					'table'                 => 'fe_users' ,
					'id_field'              => 'uid',
					'alias_field'           => 'username',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			)
		),
		'mm_forum_pi1' => array(
			array(
				'GETvar'            => 'tx_mmforum_pi1[action]',
				'valueMap'          => array(
					'themen'                => 'list_topic',
					'beitraege'             => 'list_post',
					'benutzer'              => 'forum_view_profil',
					'antworten'             => 'new_post',
					'eroeffnen'             => 'new_topic',
					'melden'                => 'post_alert',
					'bearbeiten'            => 'post_edit',
					'loeschen'              => 'post_del',
					'alle_beitraege'        => 'post_history',
					'unbeantwortet'         => 'list_unans',
					'ungelesen'             => 'list_unread',
					'alles_gelesen'         => 'reset_read',
					'abonnieren'            => 'set_havealook',
					'abo_loeschen'          => 'del_havealook',
					'favorit'               => 'set_favorite',
					'kein_favorit'          => 'del_favorite',
					'praefix'               => 'list_prefix',
					'anhaenge'              => 'get_attachment',
				),
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[fid]',
				'lookUpTable'       => array(
					'table'                 => 'tx_mmforum_forums' ,
					'id_field'              => 'uid',
					'alias_field'           => 'forum_name',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[tid]',
				'lookUpTable'       => array(
					'table'                 => 'tx_mmforum_topics' ,
					'id_field'              => 'uid',
					'alias_field'           => 'topic_title',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[pid]',
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[page]',
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[user_id]',
				'lookUpTable'       => array(
					'table'                 => 'fe_users' ,
					'id_field'              => 'uid',
					'alias_field'           => 'username',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			),
			array(
				'GETvar'            => 'tx_mmforum_pi1[user_id]',
				'lookUpTable'       => array(
					'table'                 => 'fe_users' ,
					'id_field'              => 'uid',
					'alias_field'           => 'username',
					'addWhereClause'        => ' AND NOT deleted',
					'useUniqueCache'        => 1,
					'useUniqueCache_conf'   => array(
						'strtolower'            => 1,
						'spaceCharacter'        => '_',
					),
				),
			),

		),
		'19' => 'mm_forum_pi1',
		'23' => 'mm_forum_pi3'
	),

    'postVarSets' => array(
        '_DEFAULT' => array(
            'period' => array (
                array (
                    'condPrevValue' => -1,
                    'GETvar' => 'tx_ttnews[pS]',
                ),
                array (
                    'GETvar' => 'tx_ttnews[pL]',
                ),
                array (
                    'GETvar' => 'tx_ttnews[arc]',
                    'valueMap' => array(
                        'non-archived' => -1,
                    ),
                ),
            ),

            'browse' => array (
                array (
                    'GETvar' => 'tx_ttnews[pointer]',
                ),
            ),

            'select' => array (
                array (
                    'GETvar' => 'tx_ttnews[cat]',
                    'lookUpTable' => array (
                        'table' => 'tt_news_cat',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause'=> 'AND NOT deleted',
                        'useUniqueCache'=> 1,
                        'useUniqueCache_conf' => array (
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ),
                    ),
                ),
            ),

            'article' => array(
                array (
                    'GETvar' => 'tx_ttnews[tt_news]',
                    'lookUpTable' => array (
                        'table' => 'tt_news',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'maxLength' => 12,
                        'addWhereClause'=> 'AND NOT deleted',
                        'useUniqueCache'=> 1,
                        'useUniqueCache_conf' => array (
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ),
                    ),
                ),
            ),

            'date' => array(
                array('GETvar' => 'tx_ttnews[year]' , ),
                array('GETvar' => 'tx_ttnews[month]' , ),
                array('GETvar' => 'tx_ttnews[day]' , ),
                array(
                    'GETvar' => 'tx_ttnews[tt_news]',
                    'lookUpTable' => array(
                        'table' => 'tt_news',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => array(
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ),
                    ),
                ),
            ),

            'category' => array (
                array(
                    'GETvar' => 'tx_ttnews[cat]',
                    'lookUpTable' => array(
                        'table' => 'tt_news_cat',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => array(
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ),
                    ),
                ),
            ),

        ),
    ),
);
?>