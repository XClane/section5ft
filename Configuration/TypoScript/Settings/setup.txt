[GLOBAL]

tt_content.stdWrap.innerWrap >

############  Page #############
page.includeCSS.bootstrap = EXT:section5ft/Resources/Public/css/bootstrap.min.css
page.includeCSS.bootstraptheme = EXT:section5ft/Resources/Public/css/bootstrap-theme.min.css
page.includeCSS.jssor = EXT:section5ft/Resources/Public/css/jssor.css
page.includeCSS.bxslider = EXT:section5ft/Resources/Public/css/jquery.bxslider.css
page.includeCSS.fancybox = EXT:section5ft/Resources/Public/css/jquery.fancybox.css
page.includeCSS.animate = EXT:section5ft/Resources/Public/css/animate.css
page.includeCSS.tsstatus = EXT:section5ft/Resources/Public/css/tsstatus.css
page.includeCSS.style = EXT:section5ft/Resources/Public/css/style.css

page.includeJS.jquery = EXT:section5ft/Resources/Public/js/jquery-1.9.1.min.js
page.includeJS.jquery.forceOnTop = 1
page.includeJS.bootstrap = EXT:section5ft/Resources/Public/js/bootstrap.min.js
page.includeJS.fancybox = EXT:section5ft/Resources/Public/js/jquery.fancybox.js
page.includeJS.bxslider = EXT:section5ft/Resources/Public/js/jquery.bxslider.js
page.includeJS.jssorcore = EXT:section5ft/Resources/Public/js/jssor.core.js
page.includeJS.jssorutils = EXT:section5ft/Resources/Public/js/jssor.utils.js
page.includeJS.jssorslider = EXT:section5ft/Resources/Public/js/jssor.slider.min.js
page.includeJS.superfish = EXT:section5ft/Resources/Public/js/superfish.js
page.includeJS.tsstatus = EXT:section5ft/Resources/Public/js/tsstatus.js
page.includeJS.markitup = EXT:section5ft/Resources/Public/js/jquery.markitup.js


config {
 spamProtectEmailAddresses = -3
 spamProtectEmailAddresses_atSubst = [at]
}

[globalVar = TSFE:id = 17]
page.includeJS.main = EXT:section5ft/Resources/Public/js/main.js
[globalVar = TSFE:id != 17]
page.includeJS.functions = EXT:section5ft/Resources/Public/js/functions.js
[global]


[PIDupinRootline = 6]
# Head (komplett) und body-tag werden von allen Unterseiten entfernt
config.disableAllHeaderCode = 1
[GLOBAL]

