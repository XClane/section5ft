<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Section5-fluidtemplate');

Tx_Flux_Core::registerProviderExtensionKey('section5ft', 'Page');
Tx_Flux_Core::registerProviderExtensionKey('section5ft', 'Content');

?>