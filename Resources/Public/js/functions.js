jQuery(document).ready(function(){

	$('.slider').bxSlider({
		mode: 'fade',
		auto: true,
		speed: 600,
		infiniteLoop: true
	});

    $(".fancybox").fancybox({
        helpers: {
            title : null,
            overlay : {
                speedOut : 100
            }
        }
    });

    var classes = [ 'animated fadeInUp', 'animated fadeInDown', 'animated fadeInLeft', 'animated fadeInRight' ];
    $(".img-responsive").each(function(){
        $(this).addClass(classes[~~(Math.random()*classes.length)]);
    });
});


equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
};

$(window).load(function() {
    equalheight('.news_container .subframe');
});
$(window).resize(function(){
    equalheight('.news_container .subframe');
});
