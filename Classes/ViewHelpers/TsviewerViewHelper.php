<?php

class Tx_section5ft_ViewHelpers_TsviewerViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {

    /**
     *
     * @param string $serverip
     * @param string $serverport
     * @return
     */
    public function render($serverip, $serverport) {
        require_once("tsstatus/tsstatus.php");
        $tsstatus = new TSStatus($serverip, 10011);
        $tsstatus->useServerPort($serverport);
        $tsstatus->imagePath = "typo3conf/ext/Section5ft/Classes/ViewHelpers/tsstatus/img/";
        $tsstatus->timeout = 2;
        $tsstatus->setCache(120);
        $tsstatus->hideEmptyChannels = false;
        $tsstatus->hideParentChannels = false;
        $tsstatus->showNicknameBox = true;
        $tsstatus->showPasswordBox = false;
        return $tsstatus->render();
    }

}

?>